<?php

declare(strict_types=1);

namespace RoutingManager\Tests\UseCases;

use PHPUnit\Framework\TestCase;
use RoutingManager\Routes\Application\DisableRoute\DisableRouteCommand;
use RoutingManager\Routes\Application\DisableRoute\DisableRouteHandler;
use RoutingManager\Routes\Domain\Route;
use RoutingManager\Routes\Domain\RoutesReadRepositoryInterface;
use RoutingManager\Routes\Domain\RoutesWriteRepositoryInterface;

/**
 * @group use-cases
 */
class DisableRouteTest extends TestCase
{
    private const INDEX = 0;
    private const DESTINATION = '192.168.20.2';
    private const GATEWAY = 'link#19';
    private const FLAGS = 'UH';
    private const NETIF = 'lo0';
    private const EXPIRE = null;
    private const STATUS = 'enabled';

    /** @test */
    public function shouldDisableRoute(): void
    {
        $routeReadRepository = $this->createMock(RoutesReadRepositoryInterface::class);
        $routeWriteRepository = $this->createMock(RoutesWriteRepositoryInterface::class);

        $command = DisableRouteCommand::fromHttpRequest(1);

        $routeReadRepository->expects($this->once())
            ->method('findByIndex')
            ->with($command->index())
            ->willReturn($this->mockRouteWithExistingIndex());

        $routeWriteRepository->expects($this->once())
            ->method('disableRoute');

        $handler = new DisableRouteHandler(
            $routeReadRepository,
            $routeWriteRepository
        );

        $handler->handle($command);
    }

    /** @test */
    public function shouldThrowExceptionIfNoRouteWasFound(): void
    {
        $routeReadRepository = $this->createMock(RoutesReadRepositoryInterface::class);
        $routeWriteRepository = $this->createMock(RoutesWriteRepositoryInterface::class);

        $command = DisableRouteCommand::fromHttpRequest(1);

        $routeReadRepository->expects($this->once())
            ->method('findByIndex')
            ->with($command->index())
            ->willReturn(null);

        $routeWriteRepository->expects($this->never())
            ->method('disableRoute');

        $handler = new DisableRouteHandler(
            $routeReadRepository,
            $routeWriteRepository
        );

        $this->expectExceptionCode(404);

        $handler->handle($command);
    }

    private function mockRouteWithExistingIndex(): Route
    {
        return Route::fromPrimitives(
            self::INDEX,
            self::DESTINATION,
            self::GATEWAY,
            self::FLAGS,
            self::NETIF,
            self::EXPIRE,
            self::STATUS
        );
    }
}

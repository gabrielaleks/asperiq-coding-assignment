<?php

declare(strict_types=1);

namespace RoutingManager\Tests\UseCases;

use PHPUnit\Framework\TestCase;
use RoutingManager\Routes\Application\DeleteRoute\DeleteRouteCommand;
use RoutingManager\Routes\Application\DeleteRoute\DeleteRouteHandler;
use RoutingManager\Routes\Domain\Route;
use RoutingManager\Routes\Domain\RoutesReadRepositoryInterface;
use RoutingManager\Routes\Domain\RoutesWriteRepositoryInterface;

/**
 * @group use-cases
 */
class DeleteRouteTest extends TestCase
{
    private const INDEX = 0;
    private const DESTINATION = '192.168.20.2';
    private const GATEWAY = 'link#19';
    private const FLAGS = 'UH';
    private const NETIF = 'lo0';
    private const EXPIRE = null;
    private const STATUS = 'enabled';

    /** @test */
    public function shouldDeleteRoute(): void
    {
        $routeReadRepository = $this->createMock(RoutesReadRepositoryInterface::class);
        $routeWriteRepository = $this->createMock(RoutesWriteRepositoryInterface::class);

        $command = DeleteRouteCommand::fromHttpRequest(1);

        $routeReadRepository->expects($this->once())
            ->method('findByIndex')
            ->with($command->index())
            ->willReturn($this->mockRouteWithExistingIndex());

        $routeWriteRepository->expects($this->once())
            ->method('deleteRoute');

        $handler = new DeleteRouteHandler(
            $routeReadRepository,
            $routeWriteRepository
        );

        $handler->handle($command);
    }

    /** @test */
    public function shouldThrowExceptionIfNoRouteWasFound(): void
    {
        $routeReadRepository = $this->createMock(RoutesReadRepositoryInterface::class);
        $routeWriteRepository = $this->createMock(RoutesWriteRepositoryInterface::class);

        $command = DeleteRouteCommand::fromHttpRequest(1);

        $routeReadRepository->expects($this->once())
            ->method('findByIndex')
            ->with($command->index())
            ->willReturn(null);

        $routeWriteRepository->expects($this->never())
            ->method('deleteRoute');

        $handler = new DeleteRouteHandler(
            $routeReadRepository,
            $routeWriteRepository
        );

        $this->expectExceptionCode(404);

        $handler->handle($command);
    }

    private function mockRouteWithExistingIndex(): Route
    {
        return Route::fromPrimitives(
            self::INDEX,
            self::DESTINATION,
            self::GATEWAY,
            self::FLAGS,
            self::NETIF,
            self::EXPIRE,
            self::STATUS
        );
    }
}

<?php

declare(strict_types=1);

namespace RoutingManager\Tests\Unit\ValueObject;

use DomainException;
use PHPUnit\Framework\TestCase;
use RoutingManager\Routes\Domain\ValueObject\Flags;

/**
 * @group unit
 */
class FlagsTest extends TestCase
{
    private const VALID_FLAGS = 'UHS';
    private const INVALID_FLAGS = '1S';

    /** @test */
    public function shouldCreateFromString(): void
    {
        $flags = Flags::fromString(self::VALID_FLAGS);
        $this->assertEquals(
            self::VALID_FLAGS,
            $flags->toString()
        );
    }

    /** @test */
    public function shouldAssertValidFlags(): void
    {
        $isValid = Flags::assertIsValidFlagGroup(
            self::VALID_FLAGS
        );
        $this->assertTrue($isValid);

        $isInvalid = Flags::assertIsValidFlagGroup(
            self::INVALID_FLAGS
        );
        $this->assertFalse($isInvalid);
    }

    /** @test */
    public function shouldThrowDomainExceptionIfInvalidFlags(): void
    {
        $this->expectException(DomainException::class);
        Flags::fromString(self::INVALID_FLAGS);
    }
}

<?php

declare(strict_types=1);

namespace RoutingManager\Tests\Unit\ValueObject;

use DomainException;
use PHPUnit\Framework\TestCase;
use RoutingManager\Routes\Domain\ValueObject\Status;

/**
 * @group unit
 */
class StatusTest extends TestCase
{
    private const VALID_STATUS = 'enabled';
    private const INVALID_STATUS = 'no status';

    /** @test */
    public function shouldCreateFromString(): void
    {
        $status = Status::fromString(self::VALID_STATUS);
        $this->assertEquals(
            self::VALID_STATUS,
            $status->toString()
        );
    }

    /** @test */
    public function shouldAssertValidStatus(): void
    {
        $isValid = Status::assertIsValidStatus(
            self::VALID_STATUS
        );
        $this->assertTrue($isValid);

        $isInvalid = Status::assertIsValidStatus(
            self::INVALID_STATUS
        );
        $this->assertFalse($isInvalid);
    }

    /** @test */
    public function shouldThrowDomainExceptionIfInvalidStatus(): void
    {
        $this->expectException(DomainException::class);
        Status::fromString(self::INVALID_STATUS);
    }
}

<?php

declare(strict_types=1);

namespace RoutingManager\Tests\Unit\ValueObject;

use DomainException;
use PHPUnit\Framework\TestCase;
use RoutingManager\Routes\Domain\ValueObject\Index;

/**
 * @group unit
 */
class IndexTest extends TestCase
{
    private const VALID_INDEX = 8;
    private const INVALID_INDEX = -134;

    /** @test */
    public function shouldCreateFromInteger(): void
    {
        $integer = Index::fromInteger(self::VALID_INDEX);
        $this->assertEquals(
            self::VALID_INDEX,
            $integer->toInteger()
        );
    }

    /** @test */
    public function shouldAssertValidIndex(): void
    {
        $isValid = Index::assertIsValidIndex(
            self::VALID_INDEX
        );
        $this->assertTrue($isValid);

        $isInvalid = Index::assertIsValidIndex(
            self::INVALID_INDEX
        );
        $this->assertFalse($isInvalid);
    }

    /** @test */
    public function shouldThrowDomainExceptionIfInvalidIndex(): void
    {
        $this->expectException(DomainException::class);
        Index::fromInteger(self::INVALID_INDEX);
    }
}

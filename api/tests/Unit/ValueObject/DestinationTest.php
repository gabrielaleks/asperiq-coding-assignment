<?php

declare(strict_types=1);

namespace RoutingManager\Tests\Unit\ValueObject;

use DomainException;
use PHPUnit\Framework\TestCase;
use RoutingManager\Routes\Domain\ValueObject\Destination;

/**
 * @group unit
 */
class DestinationTest extends TestCase
{
    private const VALID_DESTINATION_WITHOUT_SUBNET = '192.168.20.211';
    private const VALID_DESTINATION_WITH_SUBNET = '192.168.10.0/24';
    private const INVALID_DESTINATION = 'invalid';

    /** @test */
    public function shouldCreateFromString(): void
    {
        $destination = Destination::fromString(self::VALID_DESTINATION_WITH_SUBNET);
        $this->assertEquals(
            self::VALID_DESTINATION_WITH_SUBNET,
            $destination->toString()
        );
    }

    /** @test */
    public function shouldAssertValidDestinationWithoutSubnet(): void
    {
        $isValid = Destination::assertIsValidAddressWithoutSubnetMasks(
            self::VALID_DESTINATION_WITHOUT_SUBNET
        );
        $this->assertTrue($isValid);
    }

    /** @test */
    public function shouldAssertValidDestinationWithSubnet(): void
    {
        $isValid = Destination::assertIsValidAddressWithSubnetMasks(
            self::VALID_DESTINATION_WITH_SUBNET
        );
        $this->assertTrue($isValid);

        $isInvalid = Destination::assertIsValidAddressWithSubnetMasks(
            self::INVALID_DESTINATION
        );
        $this->assertFalse($isInvalid);
    }

    /** @test */
    public function shouldThrowDomainExceptionIfInvalidDestination(): void
    {
        $this->expectException(DomainException::class);
        Destination::fromString(self::INVALID_DESTINATION);
    }
}

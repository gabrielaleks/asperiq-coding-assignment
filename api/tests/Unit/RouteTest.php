<?php

declare(strict_types=1);

namespace RoutingManager\Tests\Unit;

use PHPUnit\Framework\TestCase;
use RoutingManager\Routes\Domain\Route;

/**
 * @group unit
 */
class RouteTest extends TestCase
{
    /** @test */
    public function shouldCreateFromPrimitives(): void
    {
        $route = $this->createRoute();

        $this->assertEquals(1, $route->index()->toInteger());
        $this->assertEquals('127.0.0.1', $route->destination()->toString());
        $this->assertEquals('link#5', $route->gateway());
        $this->assertEquals('US', $route->flags()->toString());
        $this->assertEquals('lo0', $route->netif());
        $this->assertEquals(null, $route->expire());
        $this->assertEquals('enabled', $route->status()->toString());
    }

    /** @test */
    public function shouldDisableStatus(): void
    {
        $route = $this->createRoute();
        $route->disable();
        $this->assertEquals(
            'disabled',
            $route->status()->toString()
        );
    }

    private function createRoute(): Route
    {
        return Route::fromPrimitives(
            1,
            '127.0.0.1',
            'link#5',
            'US',
            'lo0',
            null,
            'enabled'
        );
    }
}

<?php

declare(strict_types=1);

namespace RoutingManager\Tests\Integration\Persistence;

use DI\ContainerBuilder;
use RoutingManager\Routes\Dependencies as RoutesDependencies;
use PHPUnit\Framework\TestCase;
use RoutingManager\Routes\Application\GetRoutesDetails\Routes;
use RoutingManager\Routes\Application\GetRoutesDetails\RoutesViewRepositoryInterface;
use RoutingManager\Routes\Domain\Route;
use RoutingManager\Routes\Domain\RoutesReadRepositoryInterface;
use RoutingManager\Routes\Domain\RoutesWriteRepositoryInterface;

/**
 * @group integration
 */
class CsvRoutesRepositoryTest extends TestCase
{
    private $routingTableFilePath;
    private $routesStatusesFilePath;
    private const INDEX = 0;
    private const DESTINATION = '192.168.20.2';
    private const GATEWAY = 'link#19';
    private const FLAGS = 'UH';
    private const NETIF = 'lo0';
    private const EXPIRE = null;
    private const STATUS = 'enabled';


    public static $container;

    public static function setUpBeforeClass(): void
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions(RoutesDependencies::definitions());
        self::$container = $containerBuilder->build();
    }

    public function setUp(): void
    {
        $this->routingTableFilePath = getenv('ROUTING_TABLE_FILE_PATH') ?: '/var/www/html/asperiq/api/data/routing-table.csv';
        $this->routesStatusesFilePath = getenv('ROUTES_STATUSES_FILE_PATH') ?: '/var/www/html/asperiq/api/data/routes-statuses.csv';

        $this->addFixtureToCsv($this->routingTableFilePath);
        $this->addFixtureToCsv($this->routesStatusesFilePath);
    }

    private function addFixtureToCsv(string $filePath): void
    {
        $existingContent = [];
        $existingContent = file($filePath);

        $header = $existingContent[0];
        unset($existingContent[0]);

        if ($filePath == $this->routingTableFilePath) {
            $fixture = self::DESTINATION . ',' . self::GATEWAY . ',' . self::FLAGS . ',' . self::NETIF . ',' . self::EXPIRE . "\n";
        } else {
            $fixture = self::STATUS . "\n";
        }

        $file = fopen($filePath, 'w');
        fwrite($file, $header);
        fwrite($file, $fixture);

        foreach ($existingContent as $line) {
            fwrite($file, $line);
        }

        fclose($file);
    }

    /** @test */
    public function shouldFindRoutesDetails(): void
    {
        /** @var RoutesViewRepositoryInterface $viewRepository  */
        $viewRepository = self::$container->get(RoutesViewRepositoryInterface::class);
        $routes = $viewRepository->findRoutesDetails();
        $this->assertInstanceOf(Routes::class, $routes);
    }

    /** @test */
    public function shouldFindByIndex(): void
    {
        /** @var RoutesReadRepositoryInterface $readRepository  */
        $readRepository = self::$container->get(RoutesReadRepositoryInterface::class);
        $route = $readRepository->findByIndex(self::INDEX);
        $this->assertInstanceOf(Route::class, $route);
        $this->assertEquals(self::DESTINATION, $route->destination()->toString());
        $this->assertEquals(self::GATEWAY, $route->gateway());
        $this->assertEquals(self::FLAGS, $route->flags()->toString());
        $this->assertEquals(self::NETIF, $route->netif());
        $this->assertEquals(self::EXPIRE, $route->expire());
        $this->assertEquals(self::STATUS, $route->status()->toString());
    }

    /** @test */
    public function shouldDisableRoute(): void
    {
        $route = Route::fromPrimitives(
            self::INDEX,
            self::DESTINATION,
            self::GATEWAY,
            self::FLAGS,
            self::NETIF,
            self::EXPIRE,
            'disabled'
        );

        /** @var RoutesWriteRepositoryInterface $writeRepository  */
        $writeRepository = self::$container->get(RoutesWriteRepositoryInterface::class);
        $writeRepository->disableRoute($route);

        /** @var RoutesReadRepositoryInterface $readRepository  */
        $readRepository = self::$container->get(RoutesReadRepositoryInterface::class);
        $route = $readRepository->findByIndex(self::INDEX);

        $this->assertEquals('disabled', $route->status()->toString());
    }

    /** @test */
    public function shouldDeleteRoute(): void
    {
        $this->addFixtureToCsv($this->routingTableFilePath);
        $this->addFixtureToCsv($this->routesStatusesFilePath);

        $netif = 'netif-for-deleted-route';
        $route = Route::fromPrimitives(
            self::INDEX,
            self::DESTINATION,
            self::GATEWAY,
            self::FLAGS,
            $netif,
            self::EXPIRE,
            self::STATUS
        );

        /** @var RoutesWriteRepositoryInterface $writeRepository  */
        $writeRepository = self::$container->get(RoutesWriteRepositoryInterface::class);
        $writeRepository->deleteRoute($route);

        /** @var RoutesReadRepositoryInterface $readRepository  */
        $readRepository = self::$container->get(RoutesReadRepositoryInterface::class);
        $route = $readRepository->findByIndex(self::INDEX);

        $this->assertNotEquals($netif, $route->netif());
    }

    public function tearDown(): void
    {
        // Remove fixture from CSV files
        $this->removeFixtureFromCsv($this->routingTableFilePath);
        $this->removeFixtureFromCsv($this->routesStatusesFilePath);
    }

    private function removeFixtureFromCsv(string $filePath): void
    {
        $existingContent = [];
        $existingContent = file($filePath);

        unset($existingContent[1]);

        $file = fopen($filePath, 'w');

        foreach ($existingContent as $line) {
            fwrite($file, $line);
        }

        fclose($file);
    }
}

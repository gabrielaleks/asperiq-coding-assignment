<?php

namespace RoutingManager\Routes\Domain;

interface RoutesWriteRepositoryInterface
{
    public function deleteRoute(Route $route): void;
    public function disableRoute(Route $route): void;
}

<?php

namespace RoutingManager\Routes\Domain;

interface RoutesReadRepositoryInterface
{
    public function findByIndex(int $index): ?Route;
}

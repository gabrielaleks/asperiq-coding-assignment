<?php

declare(strict_types=1);

namespace RoutingManager\Routes\Domain\ValueObject;

class Flags
{
    private const VALID_FLAGS = [
        'U',
        'H',
        'S',
        'R',
        'G'
    ];

    private $flags;

    private function __construct(string $flags)
    {
        $flags = strtoupper($flags);

        if (!self::assertIsValidFlagGroup($flags)) {
            throw new \DomainException('Invalid flags: ' . $flags);
        }

        $this->flags = $flags;
    }

    public static function assertIsValidFlagGroup(string $flags): bool
    {
        for ($i = 0; $i < strlen($flags); $i++) {
            if (!self::assertIsValidFlag($flags[$i])) {
                return false;
            }
        }
        return true;
    }

    public static function assertIsValidFlag(string $flag): bool
    {
        return in_array($flag, self::VALID_FLAGS) ? true : false;
    }

    public static function fromString(string $flags): self
    {
        return new self($flags);
    }

    public function toString(): string
    {
        return (string) $this->flags;
    }
}

<?php

declare(strict_types=1);

namespace RoutingManager\Routes\Domain\ValueObject;

class Status
{
    private const VALID_STATUS = [
        'enabled',
        'disabled'
    ];

    private $status;

    private function __construct(string $status)
    {
        $status = strtolower($status);
        if (!self::assertIsValidStatus($status)) {
            throw new \DomainException('Invalid status: ' . $status);
        }
        $this->status = $status;
    }

    public static function assertIsValidStatus(string $status): bool
    {
        return in_array($status, self::VALID_STATUS) ? true : false;
    }

    public static function fromString(string $status): self
    {
        return new self($status);
    }

    public function toString(): string
    {
        return (string) $this->status;
    }

    public static function disabled(): self
    {
        return new self('disabled');
    }
}

<?php

declare(strict_types=1);

namespace RoutingManager\Routes\Domain\ValueObject;

class Destination
{
    private $destination;

    private function __construct(string $destination)
    {
        if (!self::assertValidDestination($destination)) {
            throw new \DomainException('Invalid destination: ' . $destination);
        }
        $this->destination = $destination;
    }

    public static function assertValidDestination(string $destination): bool
    {
        if (
            self::assertIsValidAddressWithSubnetMasks($destination) ||
            self::assertIsValidAddressWithoutSubnetMasks($destination)
        ) {
            return true;
        }
        return false;
    }

    public static function assertIsValidAddressWithoutSubnetMasks(string $destination): bool
    {
        $isValid = filter_var($destination, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) !== false;
        return $isValid;
    }

    public static function assertIsValidAddressWithSubnetMasks(string $destination): bool
    {
        $pattern = '/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\/(3[0-2]|[1-2]?[0-9])$/';
        return preg_match($pattern, $destination) === 1;
    }

    public static function fromString(string $destination): self
    {
        return new self($destination);
    }

    public function toString(): string
    {
        return (string) $this->destination;
    }
}

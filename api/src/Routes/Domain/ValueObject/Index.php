<?php

declare(strict_types=1);

namespace RoutingManager\Routes\Domain\ValueObject;

class Index
{
    private $index;

    private function __construct(int $index)
    {
        if (!self::assertIsValidIndex($index)) {
            throw new \DomainException('Invalid index: ' . $index);
        }

        $this->index = $index;
    }

    public static function assertIsValidIndex(int $index): bool
    {
        return $index >= 0 ? true : false;
    }

    public static function fromString(string $index)
    {
        return new static((int) $index);
    }

    public static function fromInteger(int $index): self
    {
        return new static($index);
    }

    public function toInteger(): int
    {
        return $this->index;
    }

    public function toString(): string
    {
        return (string) $this->index;
    }
}

<?php

declare(strict_types=1);

namespace RoutingManager\Routes\Domain;

use RoutingManager\Routes\Domain\ValueObject\Destination;
use RoutingManager\Routes\Domain\ValueObject\Flags;
use RoutingManager\Routes\Domain\ValueObject\Index;
use RoutingManager\Routes\Domain\ValueObject\Status;

class Route implements \JsonSerializable
{
    private $index;
    private $destination;
    private $gateway;
    private $flags;
    private $netif;
    private $expire;
    private $status;

    private function __construct(
        Index $index,
        Destination $destination,
        string $gateway,
        Flags $flags,
        string $netif,
        ?string $expire,
        Status $status
    ) {
        $this->index = $index;
        $this->destination = $destination;
        $this->gateway = $gateway;
        $this->flags = $flags;
        $this->netif = $netif;
        $this->expire = $expire;
        $this->status = $status;
    }

    public static function fromPrimitives(
        int $index,
        string $destination,
        string $gateway,
        string $flags,
        string $netif,
        ?string $expire,
        string $status
    ): self {
        return new self(
            Index::fromInteger($index),
            Destination::fromString($destination),
            $gateway,
            Flags::fromString($flags),
            $netif,
            $expire,
            Status::fromString($status)
        );
    }

    public function index(): Index
    {
        return $this->index;
    }

    public function destination(): Destination
    {
        return $this->destination;
    }

    public function gateway(): string
    {
        return $this->gateway;
    }

    public function flags(): Flags
    {
        return $this->flags;
    }

    public function netif(): string
    {
        return $this->netif;
    }

    public function expire(): ?string
    {
        return $this->expire;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function disable(): void
    {
        $this->status = Status::disabled();
    }

    public function toArray(): array
    {
        return [
            'index' => $this->index->toString(),
            'destination' => $this->destination->toString(),
            'gateway' => $this->gateway,
            'flags' => $this->flags->toString(),
            'netif' => $this->netif,
            'expire' => $this->expire,
            'status' => $this->status->toString()
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}

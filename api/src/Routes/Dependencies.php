<?php

namespace RoutingManager\Routes;

use RoutingManager\Routes\Application\GetRoutesDetails\RoutesViewRepositoryInterface;
use RoutingManager\Routes\Domain\RoutesReadRepositoryInterface;
use RoutingManager\Routes\Domain\RoutesWriteRepositoryInterface;
use RoutingManager\Routes\Infrastructure\Persistence\CsvRoutesRepository;

final class Dependencies
{
    /**
     * The real world application would swap the CsvRoutesRepository for the
     * repository responsible for accessing the binary file in the system (possibly
     * using APIs provided by a system application). Only the infrastructure layer would
     * need to change.
     */
    public static function definitions(): array
    {
        return [
            RoutesViewRepositoryInterface::class => function () {
                return new CsvRoutesRepository();
            },
            RoutesReadRepositoryInterface::class => function () {
                return new CsvRoutesRepository();
            },
            RoutesWriteRepositoryInterface::class => function () {
                return new CsvRoutesRepository();
            }
        ];
    }
}

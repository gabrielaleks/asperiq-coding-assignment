<?php

declare(strict_types=1);

namespace RoutingManager\Routes\Infrastructure\Web;

use Exception;
use RoutingManager\Routes\Application\DeleteRoute\DeleteRouteCommand;
use RoutingManager\Routes\Application\DeleteRoute\DeleteRouteHandler;
use RoutingManager\Routes\Application\DisableRoute\DisableRouteCommand;
use RoutingManager\Routes\Application\DisableRoute\DisableRouteHandler;
use RoutingManager\Routes\Application\GetRoutesDetails\RoutesViewRepositoryInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Controller
{
    private $routesViewRepository;
    private $deleteRouteHandler;
    private $disableRouteHandler;

    public function __construct(
        RoutesViewRepositoryInterface $routesViewRepository,
        DeleteRouteHandler $deleteRouteHandler,
        DisableRouteHandler $disableRouteHandler
    ) {
        $this->routesViewRepository = $routesViewRepository;
        $this->deleteRouteHandler = $deleteRouteHandler;
        $this->disableRouteHandler = $disableRouteHandler;
    }

    public function findRoutes(Request $request, Response $response): Response
    {
        $routes = $this->routesViewRepository->findRoutesDetails();

        if (!$routes) {
            throw new HttpNotFoundException($request, 'There are no routes available!');
        }

        $response->getBody()->write(json_encode(['routes' => $routes], JSON_UNESCAPED_SLASHES));
        return $response->withAddedHeader('Content-Type', 'application/json');
    }

    public function deleteRoute(Request $request, Response $response, array $args): Response
    {
        $index = (int) $args['i'];

        $command = DeleteRouteCommand::fromHttpRequest($index);

        try {
            $this->deleteRouteHandler->handle($command);
        } catch (Exception $e) {
            if ($e->getCode() == 404) {
                throw new HttpNotFoundException($request, $e->getMessage());
            } else {
                throw new HttpBadRequestException($request, $e->getMessage());
            }
        }

        $routes = $this->routesViewRepository->findRoutesDetails();

        $response->getBody()->write(json_encode([
            'message' => 'Route successfully deleted!',
            'routes' => $routes
        ], JSON_UNESCAPED_SLASHES));

        return $response->withAddedHeader('Content-Type', 'application/json');
    }

    public function disableRoute(Request $request, Response $response, array $args): Response
    {
        $index = (int) $args['i'];

        $command = DisableRouteCommand::fromHttpRequest($index);

        try {
            $this->disableRouteHandler->handle($command);
        } catch (Exception $e) {
            if ($e->getCode() == 404) {
                throw new HttpNotFoundException($request, $e->getMessage());
            } else {
                throw new HttpBadRequestException($request, $e->getMessage());
            }
        }

        $routes = $this->routesViewRepository->findRoutesDetails();

        $response->getBody()->write(json_encode([
            'message' => 'Route successfully disabled!',
            'routes' => $routes
        ], JSON_UNESCAPED_SLASHES));

        return $response->withAddedHeader('Content-Type', 'application/json');
    }
}

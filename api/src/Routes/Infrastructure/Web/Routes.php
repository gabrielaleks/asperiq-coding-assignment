<?php

declare(strict_types=1);

namespace RoutingManager\Routes\Infrastructure\Web;

use Slim\App;

class Routes
{
    public static function addRoutes(App $app): void
    {
        $app->get("/routes/fetch", [Controller::class, "findRoutes"]);
        $app->post("/routes/{i}/delete", [Controller::class, "deleteRoute"]);
        $app->get("/routes/{i}/disable", [Controller::class, "disableRoute"]);
    }
}

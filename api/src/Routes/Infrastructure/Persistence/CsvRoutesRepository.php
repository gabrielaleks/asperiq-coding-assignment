<?php

declare(strict_types=1);

namespace RoutingManager\Routes\Infrastructure\Persistence;

use RoutingManager\Routes\Application\GetRoutesDetails\Route as RouteDetails;
use RoutingManager\Routes\Application\GetRoutesDetails\Routes;
use RoutingManager\Routes\Application\GetRoutesDetails\RoutesViewRepositoryInterface;
use RoutingManager\Routes\Domain\Route;
use RoutingManager\Routes\Domain\RoutesReadRepositoryInterface;
use RoutingManager\Routes\Domain\RoutesWriteRepositoryInterface;

class CsvRoutesRepository implements
    RoutesViewRepositoryInterface,
    RoutesReadRepositoryInterface,
    RoutesWriteRepositoryInterface
{
    private $routingTableFilePath;
    private $routesStatusesFilePath;

    public function __construct()
    {
        $this->routingTableFilePath = getenv('ROUTING_TABLE_FILE_PATH');
        $this->routesStatusesFilePath = getenv('ROUTES_STATUSES_FILE_PATH');
    }

    public function findRoutesDetails(): ?Routes
    {
        $routesData = $this->joinEveryRouteToEquivalentStatus();

        if (!$routesData) {
            return null;
        }

        $routes = new Routes();
        foreach ($routesData as $routeData) {
            $route = RouteDetails::fromPrimitives(
                $routeData['Destination'],
                $routeData['Gateway'],
                $routeData['Flags'],
                $routeData['Netif'],
                $routeData['Expire'],
                $routeData['Status']
            );
            $routes->add($route);
        }

        return $routes;
    }

    public function findByIndex(int $index): ?Route
    {
        $routesData = $this->joinEveryRouteToEquivalentStatus();
        $routeData = $routesData[$index] ?? null;

        if (!$routeData) {
            return null;
        }

        $route = Route::fromPrimitives(
            $routeData['Index'],
            $routeData['Destination'],
            $routeData['Gateway'],
            $routeData['Flags'],
            $routeData['Netif'],
            $routeData['Expire'],
            $routeData['Status']
        );

        return $route;
    }

    public function deleteRoute(Route $route): void
    {
        $this->deleteRowFromCsvByIndex($this->routingTableFilePath, $route->index()->toInteger());
        $this->deleteRowFromCsvByIndex($this->routesStatusesFilePath, $route->index()->toInteger());
    }

    public function disableRoute(Route $route): void
    {
        if ($route->status()->toString() !== 'disabled') {
            throw new \RuntimeException('Should not update route status to value different than "disabled".');
        }

        // Fetch current data
        $data = $this->arrayifyCsv($this->routesStatusesFilePath);
        $headers = array_keys($data[0]);
        unset($headers[array_search('Index', $headers)]);

        // Update status for given index
        $data[$route->index()->toInteger()]['Status'] = $route->status()->toString();

        // Overwrite CSV file with new data
        $csvFile = fopen($this->routesStatusesFilePath, 'w');
        fputcsv($csvFile, $headers);

        foreach ($data as $row) {
            unset($row['Index']);
            fputcsv($csvFile, $row);
        }

        fclose($csvFile);
    }

    private function joinEveryRouteToEquivalentStatus(): ?array
    {
        $routingTableData = $this->arrayifyCsv($this->routingTableFilePath);
        $routesStatusesData = $this->arrayifyCsv($this->routesStatusesFilePath);
        return $this->mergeArraifiedCsvs($routingTableData, $routesStatusesData);
    }

    private function arrayifyCsv(string $csvPath): ?array
    {
        $arraifiedData = [];
        $csvFile = fopen($csvPath, "r");
        if ($csvFile !== false) {
            $headers = fgetcsv($csvFile);
            if (feof($csvFile)) {
                fclose($csvFile);
                return null;
            }

            $row = 0;
            while (($data = fgetcsv($csvFile)) !== false) {
                $arraifiedData[] = ['Index' => $row, ...array_combine($headers, $data)];
                $row++;
            }
        }
        return $arraifiedData;
    }

    private function mergeArraifiedCsvs(array $routingTable, array $routesStatuses): array
    {
        $mergedData = [];
        foreach ($routingTable as $index => $routeData) {
            // Check if there is a corresponding status data for this route index
            if (isset($routesStatuses[$index])) {
                $mergedData[] = array_merge($routeData, $routesStatuses[$index]);
            } else {
                // If no status data found, throw exception
                throw new \InvalidArgumentException('Route with index ' . $index . ' does not have a valid status!');
            }
        }

        return $mergedData;
    }

    private function deleteRowFromCsvByIndex(string $csvPath, int $index): void
    {
        // Fetch current data
        $data = $this->arrayifyCsv($csvPath);
        $headers = array_keys($data[0]);
        unset($headers[array_search('Index', $headers)]);

        // Remove row from array
        unset($data[$index]);

        // Overwrite CSV file with new data
        $csvFile = fopen($csvPath, 'w');
        fputcsv($csvFile, $headers);

        foreach ($data as $row) {
            unset($row['Index']);
            fputcsv($csvFile, $row);
        }

        fclose($csvFile);
    }
}

<?php

namespace RoutingManager\Routes\Application\DeleteRoute;

use RoutingManager\Routes\Domain\RoutesReadRepositoryInterface;
use RoutingManager\Routes\Domain\RoutesWriteRepositoryInterface;

class DeleteRouteHandler
{
    private $routeReadRepository;
    private $routeWriteRepository;

    public function __construct(
        RoutesReadRepositoryInterface $routeReadRepository,
        RoutesWriteRepositoryInterface $routeWriteRepository
    ) {
        $this->routeWriteRepository = $routeWriteRepository;
        $this->routeReadRepository = $routeReadRepository;
    }

    public function handle(DeleteRouteCommand $command): void
    {
        $route = $this->routeReadRepository->findByIndex($command->index());

        if (!$route) {
            throw new \Exception("No route with index " . $command->index() . " found.", 404);
        }

        $this->routeWriteRepository->deleteRoute($route);
    }
}

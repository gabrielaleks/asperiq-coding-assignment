<?php

declare(strict_types=1);

namespace RoutingManager\Routes\Application\GetRoutesDetails;

class Routes implements \JsonSerializable
{
    /** @var Route[] */
    private $routes = [];

    public function add(Route $route): void
    {
        $this->routes[] = $route;
    }

    public function toArray(): array
    {
        return array_map(function (Route $route) {
            return $route->toArray();
        }, $this->routes);
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}

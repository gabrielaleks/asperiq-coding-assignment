<?php

declare(strict_types=1);

namespace RoutingManager\Routes\Application\GetRoutesDetails;

interface RoutesViewRepositoryInterface
{
    public function findRoutesDetails(): ?Routes;
}

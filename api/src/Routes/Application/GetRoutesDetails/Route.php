<?php

declare(strict_types=1);

namespace RoutingManager\Routes\Application\GetRoutesDetails;

class Route implements \JsonSerializable
{
    private $destination;
    private $gateway;
    private $flags;
    private $netif;
    private $expire;
    private $status;

    public function __construct(
        string $destination,
        string $gateway,
        string $flags,
        string $netif,
        ?string $expire,
        string $status
    ) {
        $this->destination = $destination;
        $this->gateway = $gateway;
        $this->flags = $flags;
        $this->netif = $netif;
        $this->expire = $expire;
        $this->status = $status;
    }

    public static function fromPrimitives(
        string $destination,
        string $gateway,
        string $flags,
        string $netif,
        ?string $expire,
        string $status
    ): self {
        return new self(
            $destination,
            $gateway,
            $flags,
            $netif,
            $expire,
            $status
        );
    }

    public function toArray(): array
    {
        return [
            'destination' => $this->destination,
            'gateway' => $this->gateway,
            'flags' => $this->flags,
            'netif' => $this->netif,
            'expire' => $this->expire,
            'status' => $this->status
        ];
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}

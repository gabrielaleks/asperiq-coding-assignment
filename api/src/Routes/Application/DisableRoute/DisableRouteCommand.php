<?php

namespace RoutingManager\Routes\Application\DisableRoute;

class DisableRouteCommand
{
    private $index;

    public function __construct(int $index)
    {
        $this->index = $index;
    }

    public static function fromHttpRequest(int $index): self
    {
        return new self($index);
    }

    public function index(): int
    {
        return $this->index;
    }
}

<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/dotenv.php';

use DI\ContainerBuilder;
use Slim\Exception\HttpNotFoundException;
use Slim\Factory\AppFactory;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use RoutingManager\Routes\Infrastructure\Web\Routes;
use RoutingManager\Routes\Dependencies as RoutesDependencies;

// This sleep was added to simulate delays that would occur in a real world scenario
sleep(3);

loadDotenvFile(__DIR__ . '/.env.development');
loadDotenvFile(__DIR__ . '/.env');

$containerBuilder = new ContainerBuilder();

$containerBuilder->addDefinitions(RoutesDependencies::definitions());

$container = $containerBuilder->build();

$app = AppFactory::createFromContainer($container);

$errorMiddleware = $app->addErrorMiddleware(true, true, true);

$errorMiddleware->setDefaultErrorHandler(function ($request, $exception) use ($app) {
    $response = $app->getResponseFactory()->createResponse();
    $code = $exception->getCode() ? $exception->getCode() : 400;
    $response->getBody()->write(json_encode([
        'status' => 'error',
        'code' => $code,
        'message' => $exception->getMessage(),
        'timestamp' => date('Y-m-d H:i:s'),
    ]));

    return $response->withHeader('Content-Type', 'application/json')->withStatus($code);
});

$app->setBasePath('/api');

Routes::addRoutes($app);

$app->any('/{routes:.+}', function (Request $request, Response $response) {
    throw new HttpNotFoundException($request, "Route not found.");
});

$app->run();

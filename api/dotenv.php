<?php

function loadDotenvFile($envFilePath): void
{
    if (file_exists($envFilePath)) {
        $lines = file($envFilePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        foreach ($lines as $line) {
            // Ignore lines that start with # or lines without '='
            if (strpos(trim($line), '#') === 0 || strpos(trim($line), '=') === false) {
                continue;
            }

            // Split the line by '=' to get key and value
            list($key, $value) = explode('=', $line, 2);
            $key = trim($key);
            $value = trim($value);

            // Set the environment variable
            putenv("$key=$value");
        }
    }
}

# Asperiq coding assignment

This repository contains the full stack web application that was developed as a coding assignment for the Asperiq selection process. It contains the following general structure:

```
├── api
├── app
├── .gitignore
├── .gitlab-ci.yml
└── docker-compose.yml
```

- The *api* folder contains everything backend-related: dockerfile, PHP code, tests and dependencies.
- The *app* folder contains everything frontend-related: dockerfile, HTML, resources (JS, CSS, images and compiled Bootstrap files (CSS/JS)).
- The *.gitlab-ci.yml* contains the stages and jobs for a valid GitLab CI/CD pipeline.
- The *docker-compose.yml* file sets up a development environment for two services, 'api' and 'app', each running in its own container and connected to a common network.

To start using the application, simply run `docker compose up`. After both services finish loading, access the application on http://localhost:80.

## Backend (api)

The backend consists of a PHP REST application with an Apache server listening on port 3000. This application contains three endpoints:
- GET api/routes/fetch -> returns list of available routes
- GET api/routes/{i}/disable -> disables route with index i
- POST api/routes/{i}/delete  -> deletes route with index i

### Requirements

List of requirements 

- [X] PHP should be used to implement a REST API
- [X] The REST API should be listening on port 3000
- [X] The backend retrieves data/state from a fictional binary on the system
- [X] Should have an endpoint using a GET request to fetch data from the binary with a delay of 3-5 seconds
- [X] Should have an endpoint using a POST request + index to delete a route entry with a delay of 3-5 seconds
- [X] Should have an endpoint using a GET request + index to disable a route entry with a delay of 3-5 seconds

### Decisions

Since there's no call to binary files or system applications, the system wouldn't be able to know the routes that are available and wouldn't be able to perform actions on them. The requirements document proposed doing dummy calls to show what would happen. However, to completely cover every aspect of a given implementation and present the benefits of the chosen architecture, I decided to store the routes in a CSV file. This file (api/data/routing-table.csv) has the exact same structure as the one shown in the document. Since there's also no indication of what represents a disabled route, I created another file responsible for containing the Status of a Route (api/data/routes-statuses.csv). I added a different file instead of creating a column 'status' in the first file since a real world application, as suggested, would not have the status returned in the binary call.

Another decision that I had to take was how to uniquely represent and tackle a route. None of the columns are unique, so they can't be used singularly; also, I didn't add an 'id' column for the same reason I didn't add a 'status' one. Apparently, a combination of 'destination' and 'gateway' provides an unique identifier to the route. I considered using this combination as a way of tackling a specific route, however the requirements document has an example of dummy backend shell calls that lead me to believe that a requirement would be to use the *index* of the row as an identifier. I adopted this and came up with the following constraints:
- New routes (and their statuses) are always added to the end of the csv file.
- The deletion of a route obliges the rest of the routes to re-index.

Another decision I made was to return the current list of routes as a response whenever the user disables/deletes a route. This list will be used to update the table. I did this thinking about the real world scenario: since the list of routes could be dynamic, it would be nice to have any new routes appear in the table after an action has been taken. Note that this requires an action. If a new route appears but no action is taken, the table will be outdated. A solution would be to open a websocket in order to implement realtime capability to the table.

Finally, regarding external dependencies, I used 3: 
- slim/slim + slim/psr7: these were used to facilitate the API development. I'm counting both as one since the second one is necessary for the first one to work and they are both from slim.
- php-di/php-di: This package was included to manage dependency injection.
- phpunit/phpunit: This is a development-only dependency. It was included to manage tests.

I was also going to use vlucas/phpdotenv, a package I normally use to load my environment variables, but I decided to just write the desired behavior myself.

### Architecture

A DDD (Domain-Driven Design) approach was followed. Inside api/src we have the Routes folder, which represents the Routes bounded context within our application. Inside that folder there are three folders, one representing each layer of the project: Application, Domain and Infrastructure. There, we can also find the Dependencies.php file, whose responsibility is to handle dependency-injection in our application.

The Application layer contains the use cases of our Routes context: GetRoutesDetails, DisableRoute and DeleteRoute. An use case can have interfaces for infrastructure services, commands (and their handlers) and more.

The Domain layer contains our main entity: Route. It also contains our value objects, each with its own set of checks and validations.

The Domain and Application layers, together, contain only the *core code* of our project. This means that they are not aware - and should not be - of any external dependency. They do not care *how* the list of routes was selected nor *how* a specific route will be deleted/disabled. Since these actions depend on external information/services (e.g. CSV file, database, API), these two layers use *interfaces* to operate transactions with the 'outside world'.

The Infrastructure layer is the one responsible for communicating with external services, accessing and persisting data. The Web folder is responsible for processing the incoming HTTP requests and providing a response. In the Persistence folder we add the repository responsible for reading and persisting data. Since in our application the data is stored as CSV files, a CSV repository was built.

### Tests

Tests were added to the project. Each layer is properly tested.

Unit tests were written to ensure that a specific set of business rules are correctly followed (e.g. ensuring that no invalid Flags are created).

Integration tests were written to ensure that the repository methods are working as expected (reading/deleting/disabling routes). This ensures that the CSVs are being properly updated.

Use case tests were written to ensure that the expected behavior of each use case is followed. Use case tests should not alter the CSVs, as this behavior is already tested by integration tests. To prevent that, the repository interfaces were mocked.

## Frontend (app)

The frontend consists of a static HTML page using Bootstrap to implement the UI and vanilla Javascript to do HTTP requests and DOM management.

### Requirements

List of requirements

- [X] HTML/CSS/JS should be used
- [X] Should use bootstrap for UI components
- [X] Bootstrap should be bundled up into the application (no CDN)
- [X] Should use vanilla JS
- [X] Upon loading, an asynchronous HTTP call should be made to fetch the content of the table
- [X] An asynchronous GET HTTP call should be made, upon the click of a button, to disable a route
- [X] An asynchronous POST HTTP call should be made, upon the click of a button, to delete a route
- [X] Implement good user experience for async data loading, interaction and error handling.

### Decisions

Even though it was allowed, I decided to not use JQuery. I preferred to use vanilla JS to show how the whole implementation could be done with pure javascript.

Given the mentioned constraint regarding the routes being listed in a CSV file and the unique identifier being the row number, I added a rule to prohibit simultaneous operations. That is, the user cannot click the button to delete a route after they clicked the button to delete another one and the operation still hasn't finished. This is an important rule because, without it, the following situation could happen: 
1. User clicks to delete route 4
2. User clicks to delete route 5 (route 4 is in process of being deleted)
3. Route 4 is deleted. What used to be route 5 now is route 4.
4. Route '5' is deleted. However, since the routes were re-indexed, actually the route 6 (from before the first operation) was deleted.

Regarding style, the system contains many features:
- An animated skeleton loader appears below the header of the table while the routes are being fetched.
- Rows with inactive routes appear as gray. Also, these rows have the 'Disable' button disabled.
- A spinner occupies the space of the actions cell for the clicked button row.
- It is not possible to click an action button while another operation is happening. A popover appears next to the clicked button warning the user that they should wait.
- A yellow alert appears on the top right if no route is found.
- A red alert appears on the top right if something goes wrong. The error is also logged to the console.
- The image of a router was added as the page's favicon.
- A fixed minimum distance exists between the table, header and footer. This means that the table could have 1000 records and there wouldn't be an overflow.

### Architecture

There are 3 main js files:

- event-handler.js: Handles page events and acts as the entry point for event-driven logic.
- route-service.js: Manages asynchronous HTTP requests and handles communication with the server.
- ui-renderer.js: Manages UI manipulation, such as adding, editing, or removing HTML elements.

This modular architecture enhances the code organization, readability and maintainability by keeping related functionalities isolated and focused within individual modules. It also facilitates debugging and testing since each module has a clearly defined purpose.

The file event-handler.js also maintains a store to manage the state of the application. This allows for easy access to route data without needing to fetch again from the server, enhancing performance.

## Extra: GitLab CI/CD

Since the code is hosted in GitLab, I also prepared a CI/CD configuration file.

Two stages were defined: build and test.

The build stage is responsible for preparing the application for the upcoming tests. It contains two jobs:
- build: this job is responsible for installing PHP dependencies with composer and caching them to be used during the tests.
- generate-csv: this job generates two ephemeral CSV files to be used by the integration tests.

The test stage is responsible for running the PHP tests. It contains three jobs:
- unit-test: runs unit tests.
- integration-test: runs integration tests (before, it also sets the environment variables that are to be used by the tests to find the CSV files).
- use-case-test: runs use case tests.

Having a CI/CD pipeline configured is important for a project as the automated tests ensure that code changes are thoroughly tested before being merged, thus reducing the likelihood of introducing bugs.

The CI/CD could be improved even more by including:
- Automated deployment
- Static code analysis
- Code coverage reporting

and more.
const BASE_PATH = window.location.host === 'localhost' ? 'http://localhost:3000' : 'production_value';

export default BASE_PATH;
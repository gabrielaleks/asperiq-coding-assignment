import { setLoadingIndicator, clearLoadingIndicator } from './ui-renderer.js';
import BASE_PATH from './config/config.js';

async function fetchRoutes(configuration) {
    setLoadingIndicator(configuration);

    const url = new URL('/api/routes/fetch', BASE_PATH);
    try {
        const response = await fetch(url);

        if (!response.ok && response.status !== 404) {
            throw new Error(`Failed to fetch routes from the server: HTTP status ${response.status} (${response.statusText})`);
        }
        
        clearLoadingIndicator(configuration);
        return response.json();
    } catch (error) {
        clearLoadingIndicator(configuration);
        throw error;
    }
}

async function disableRoute(index, configuration) {
    return fetchWithMethod(`/api/routes/${index - 1}/disable`, 'GET', configuration);
}

async function deleteRoute(index, configuration) {
    return fetchWithMethod(`/api/routes/${index - 1}/delete`, 'POST', configuration);
}

async function fetchWithMethod(endpoint, method, configuration) {
    setLoadingIndicator(configuration);

    const url = new URL(endpoint, BASE_PATH);

    try {
        const response = await fetch(url, { method });

        if (!response.ok) {
            throw new Error(`Failed to ${method.toLowerCase()} route from the server: HTTP status ${response.status} (${response.statusText})`);
        }

        clearLoadingIndicator(configuration);
        return response.json();
    } catch (error) {
        clearLoadingIndicator(configuration);
        throw error;
    }
}

export { 
    fetchRoutes,
    disableRoute,
    deleteRoute
};
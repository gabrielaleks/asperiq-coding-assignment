import { fetchRoutes, disableRoute, deleteRoute } from './route-service.js';
import { setTableDataWithResponse, showAlertWithMessage, showPopoverOnOverlappedAction } from './ui-renderer.js';

let store = {};
let requestInProgress = false;

document.addEventListener('DOMContentLoaded', async function () {
    try {
        const configuration = { feedbackType: 'skeletonLoader' };
        const response = await fetchRoutes(configuration);

        if (response.code === 404) {
            showAlertWithMessage(response.message, 'alert-warning');
        }
        
        let tableData = setTableDataWithResponse(response);
        store = {'routes': tableData};
    } catch (error) {
        showAlertWithMessage('Could not fetch available routes, please try again.', 'alert-danger');
        console.error(error);
    }
});

document.body.addEventListener('click', async function(event) {
    const button = event.target.closest('.disable-route-button');
    if (button && !requestInProgress) {
        const rowData = store.routes[button.dataset.rowIndex - 1];
        try {
            requestInProgress = true;
            if (rowData.status === 'disabled') {
                return;
            }

            const configuration = { feedbackType: 'spinner', button };
            let response = await disableRoute(rowData.rowIndex, configuration);

            setTableDataWithResponse(response);
        } catch (error) {
            showAlertWithMessage('Could not disable route, please try again.', 'alert-danger');
            console.error(error);
        } finally {
            requestInProgress = false;
        }
    } else if (requestInProgress) {
        showPopoverOnOverlappedAction(button);
    }
});

document.body.addEventListener('click', async function(event) {
    const button = event.target.closest('.delete-route-button');
    if (button && !requestInProgress) {
        const rowData = store.routes[button.dataset.rowIndex - 1];
        try {
            requestInProgress = true;
            const configuration = { feedbackType: 'spinner', button };
            let response = await deleteRoute(rowData.rowIndex, configuration);
            setTableDataWithResponse(response);
        } catch (error) {
            showAlertWithMessage('Could not delete route, please try again.', 'alert-danger');
            console.error(error);
        } finally {
            requestInProgress = false;
        }
    } else if (requestInProgress) {
        showPopoverOnOverlappedAction(button);
    }
});
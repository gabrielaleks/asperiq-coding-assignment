function setTableDataWithResponse(response) {
    let tableData = [];
    let rowIndex = 1;

    clearTable();

    if (!response.routes) {
        return [];
    };
    
    response.routes.forEach(function(data) {
        const rowData = { rowIndex: rowIndex, ...data};
        tableData.push(rowData);
        
        var row = table.insertRow();
        var actionCell = row.insertCell();

        applyRowStyles(row, data);
        createIndexCell(row, rowIndex);
        createDataCells(data, row);
        appendButtons(actionCell, rowData);

        rowIndex++;
    });

    table.scrollTop = table.scrollHeight;
    return tableData;
}

function clearTable() {
    // Clears every row except header
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
}

function applyRowStyles(row, data) {
    if (data.status === 'disabled') {
        row.classList.add('table-secondary', 'disabled-row');
    }
}

function createIndexCell(row, rowIndex) {
    var indexCell = row.insertCell(0);
    indexCell.textContent = rowIndex;
}

function createDataCells(data, row) {
    Object.entries(data).forEach(function([key, value], columnIndex) {
        if (key !== 'status') {
            var cell = row.insertCell(columnIndex + 1);
            cell.textContent = value === '' ? '-' : value;
        }
    });
}

function appendButtons(actionCell, rowData) {
    var disableButton = createDisableButton('Disable', rowData);
    actionCell.appendChild(disableButton);

    var deleteButton = createDeleteButton('Delete', rowData);
    actionCell.appendChild(deleteButton);
}

function createDisableButton(text, rowData) {
    return createActionButton(text, rowData, 'btn btn-secondary disable-route-button');
}

function createDeleteButton(text, rowData) {
    return createActionButton(text, rowData, 'btn btn-danger delete-route-button');
}

function createActionButton(text, rowData, buttonClass) {
    var button = document.createElement('button');
    var classes = buttonClass.split(' ');

    button.classList.add(...classes);
    button.dataset.rowIndex = rowData.rowIndex;
    button.textContent = text;

    if (classes.includes('disable-route-button') && rowData.status === 'disabled') {
        button.disabled = true;
    }

    return button;
}

function setLoadingIndicator(configuration) {
    if (configuration && configuration.feedbackType === 'skeletonLoader') {
        showSkeletonLoader();
    } else if (configuration && configuration.feedbackType === 'spinner') {
        showSpinner(configuration);
    }
}

function clearLoadingIndicator(configuration) {
    if (configuration && configuration.feedbackType === 'skeletonLoader') {
        hideSkeletonLoader();
    } else if (configuration && configuration.feedbackType === 'spinner') {
        hideSpinner(configuration);
    }
}

function showSkeletonLoader() {
    let skeletonLoader = createSkeletonLoader();
    let tableElement = document.getElementById('table');
    tableElement.insertAdjacentElement('afterend', skeletonLoader);
}

function hideSkeletonLoader() {
    let tableSkeleton = document.getElementById('table-skeleton');
    tableSkeleton.remove();
}

function createSkeletonLoader() {
    let skeletonLoader = document.createElement('div');
    skeletonLoader.setAttribute('id', 'table-skeleton');
    skeletonLoader.innerHTML = `
        <p class="placeholder-glow">
            <span class="placeholder col-4"></span>
            <span class="placeholder col-3"></span>
            <span class="placeholder col-4"></span>
            <span class="placeholder col-3"></span>
            <span class="placeholder col-4"></span>
            <span class="placeholder col-3"></span>
            <span class="placeholder col-4"></span>
            <span class="placeholder col-3"></span>
            <span class="placeholder col-4"></span>
        </p>
    `;
    return skeletonLoader;
}

function showSpinner(configuration) {
    let spinner = createSpinner();

    let td = configuration.button.parentElement;
    let originalContent = td.innerHTML;

    td.innerHTML = '';
    td.appendChild(spinner);
    
    configuration.spinner = spinner;
    configuration.originalContent = originalContent;
}

function hideSpinner(configuration) {
    let td = configuration.spinner.parentElement;
    td.innerHTML = configuration.originalContent;
}

function createSpinner() {
    let spinner = document.createElement('div');
    spinner.classList.add('spinner-border', 'text-dark');
    spinner.setAttribute('role', 'status');

    return spinner
}

function showAlertWithMessage(message, type) {
    let alert = createAlert(message, type);
    let closeButton = createCloseButton();
    
    alert.appendChild(closeButton);

    let tableElement = document.getElementById('table');
    tableElement.insertAdjacentElement('afterend', alert);
}

function createAlert(message, type) {
    let alert = document.createElement('div');
    alert.classList.add('alert', type, 'alert-dismissible', 'position-fixed', 'top-0', 'end-0', 'me-3', 'mt-3');
    alert.setAttribute('role', 'alert');
    alert.textContent = message;
    
    return alert;
}

function createCloseButton() {
    const closeButton = document.createElement('button');
    closeButton.setAttribute('type', 'button');
    closeButton.classList.add('btn-close');
    closeButton.setAttribute('data-bs-dismiss', 'alert');
    closeButton.setAttribute('aria-label', 'Close');
    return closeButton;
}

function showPopoverOnOverlappedAction(button) {
    if (button && !button.getAttribute('data-bs-toggle')) {
        button.setAttribute('data-bs-toggle', 'popover');
        button.setAttribute('data-bs-trigger', 'focus');
        button.setAttribute('title', 'Please wait!');
        button.setAttribute('data-bs-content', 'Another action is in progress...');

        const popover = new bootstrap.Popover(button);
        popover.show();
    }
}

export {
    setTableDataWithResponse,
    createDisableButton,
    createDeleteButton,
    setLoadingIndicator,
    clearLoadingIndicator,
    showAlertWithMessage,
    showPopoverOnOverlappedAction
}